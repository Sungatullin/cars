# Cars



## instruction

- `git clone https://gitlab.com/Sungatullin/cars.git` clone rep
- `docker-compose up -d` set up database.
if you aren't using docker, create it and set up `settings.py`
- `poetry shell` - init virtual environment
- `poetry install` - install dependencies
- `flask db upgrade` - upgrade data base to actual version
- `export FLASK_APP=cars`
- `flask run` 

