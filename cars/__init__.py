__version__ = '0.1.0'

from flask import Flask, session
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate

from cars import settings

db = SQLAlchemy()
migrate = Migrate()


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'cars forum qwerty'
    app.config['SQLALCHEMY_DATABASE_URI'] = \
        f'postgresql://{settings.DB_USERNAME}:{settings.DB_PASSWORD}@' \
        f'{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}'
    app.config['FLASK_APP'] = settings.FLASK_APP
    app.config['FLASK_DEBUG'] = settings.FLASK_DEBUG
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    from cars.models import User

    db.init_app(app)
    migrate.init_app(app, db)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)
    login_manager.login_message = "Вы не авторизованы"

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
