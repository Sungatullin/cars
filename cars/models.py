from flask_login import UserMixin
from cars import db
from sqlalchemy import func, text


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), default=func.now(),
                           server_default=func.now(), onupdate=func.now())


class User(UserMixin, db.Model):
    __tablename__ = 'user'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    name = db.Column(db.String(25), nullable=False)
    email = db.Column(db.String(200), nullable=False, unique=True)
    password = db.Column(db.String(200), nullable=False)
    picture = db.Column(db.String(256), default=None)

    def get_name(self):
        return f"{self.name}"

    def get_email(self):
        return f"{self.email}"


class Mainmenu(db.Model):
    __tablename__ = 'mainmenu'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    title = db.Column(db.String(25), nullable=False)
    url = db.Column(db.String(25), nullable=False)


class Car(db.Model):
    __tablename__ = "car"

    user_id = db.Column(db.INTEGER, db.ForeignKey("user.id", ondelete="CASCADE"), nullable=False)
    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    brand = db.Column(db.String(50), nullable=False)
    model = db.Column(db.String(50), nullable=False)
    generation = db.Column(db.String(50), nullable=False)
    series = db.Column(db.String(50), nullable=False)
    modification = db.Column(db.String(50), nullable=False)
    equipment = db.Column(db.String(50), nullable=False)
    information = db.Column(db.TEXT, nullable=False)
    price = db.Column(db.INTEGER, nullable=False)



