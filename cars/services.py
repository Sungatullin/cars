from sqlalchemy import text, select
from flask_login import current_user


from cars import db


def get_menu():
    with db.engine.connect() as connection:
        content = connection.execute(
            text("SELECT * from mainmenu")
        ).all()
        if content:
            return content
        else:
            return []


def get_user_cars(id):
    with db.engine.connect() as connection:
        content = connection.execute(
            text('''
            SELECT id, brand, model, generation FROM car 
            WHERE user_id = :id'''),
            id=str(id)
        ).all()
        if content:
            return content
        else:
            return []


def get_cars():
    with db.engine.connect() as connection:
        content = connection.execute(
            text('''SELECT id, brand, model, generation FROM car ''')
        ).all()
        if content:
            return content
        else:
            return []
