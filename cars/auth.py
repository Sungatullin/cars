
from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, current_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from cars.helpers import check_email, check_password
from cars import db
from cars.models import User
from cars.services import get_menu

auth = Blueprint('auth', __name__)


@auth.route("/login", methods=["POST", "GET"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.profile'))

    if request.method == "POST":
        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        user = User.query.filter_by(email=email).first()

        if not user:
            flash("Неверный логин", "error")
        elif not check_password_hash(user.password, password):
            flash("Неверный пароль", "error")
        else:
            login_user(user, remember=remember)
            return redirect(
                url_for("main.profile", email=email)
            )

    return render_template(
        'login.html',
        menu=get_menu(),
        title='Авторизация'
    )


@auth.route("/register", methods=["POST", "GET"])
def register():
    if request.method == "POST":
        email = request.form.get('email')
        name = request.form.get('name')
        password = request.form.get('password')
        password2 = request.form.get('password2')

        user = User.query.filter_by(email=email).first()

        if user:
            flash('Email занят', "error")
        elif name.isdigit():
            flash("Имя пользователя должно содержать буквы алфавита", "error")
        elif not check_email(email):
            flash("Неверный email", "error")
        elif not check_password(password):
            flash("Пароль должен превышать 4 символов и должен содержать цифры и буквы алфавита", "error")
        elif not password == password2:
            flash("Пароли не совпадают", "error")
        else:
            hash = generate_password_hash(password)
            new_user = User(email=email, name=name,
                            password=hash,)

            db.session.add(new_user)
            db.session.commit()

            return redirect(url_for('auth.login'))

    return render_template(
        'register.html',
        menu=get_menu(),
        title='Регистрация'
    )


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Вы вышли из аккаунта", "logout")
    return redirect(url_for('auth.login'))
