import json
import os
import re

from flask import Blueprint, render_template, request, redirect, flash, url_for
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash

from cars import db
from cars.models import User, Mainmenu, Car
from cars.services import get_menu, get_user_cars, get_cars
from cars.helpers import check_email, check_password

main = Blueprint('main', __name__)
UPLOAD_FILES_DIR = 'uploads'


@main.route('/', methods=['GET', 'POST'])
def index():
    return render_template(
        'index.html',
        menu=get_menu(),
        title=""
    )


@main.route("/profile")
@login_required
def profile():
    return render_template(
        'profile.html',
        title='profile',
        menu=get_menu()
    )


@main.route("/edit", methods=["POST", "GET"])
@login_required
def edit_profile():
    if request.method == "POST":
        email = request.form.get('email')
        name = request.form.get('name')
        password = request.form.get('password')
        password2 = request.form.get('password2')

        user = User.query.filter_by(email=current_user.email).first()
        user2 = User.query.filter_by(email=email).first()

        if name:
            if name.isdigit():
                flash("Имя пользователя должно содержать буквы алфавита", "error")
            elif user.name == name:
                flash("Нет изменений", "logout")
            else:
                user.name = name
                db.session.commit()
                flash("Имя пользователя изменено!", "success")

        if email:
            if user == user2:
                flash("Нет изменений", "logout")
            elif user2:
                flash('Email занят', "error")
            elif user == user2:
                pass
            elif not check_email(email):
                flash("Неверный email", "error")
            else:
                user.email = email
                db.session.commit()
                flash("Email изменен!", "success")

        if password and password2:
            if not check_password(password):
                flash("Пароль должен превышать 4 символов и должен содержать цифры и буквы алфавита", "error")
            elif not password == password2:
                flash("Пароли не совпадают", "error")
            else:
                hash = generate_password_hash(password)
                user.password = hash
                db.session.commit()
                flash("Пароль изменен!", "success")

    return render_template(
        'edit_profile.html',
        title='edit',
        menu=get_menu(),

    )


@main.app_errorhandler(404)
def page_not_found(error):
    return render_template(
        'page.html',
        title='Ошибка',
        menu=get_menu(),
        error_text='Страница не найдена'
    )


@main.route("/users_cars", methods=["POST", "GET"])
def users_cars():
    return render_template(
        'users_cars.html',
        title='edit',
        menu=get_menu(),
        cars=get_cars()
    )


@main.route("/profile/add_car", methods=["POST", "GET"])
@login_required
def add_car():
    if request.method == "POST":
        email = current_user.email
        brand = request.form.get('brand')
        model = request.form.get('model')
        generation = request.form.get('generation')
        series = request.form.get('series')
        modification = request.form.get('modification')
        equipment = request.form.get('equipment')
        information = request.form.get('information')
        price = request.form.get('price')

        user = User.query.filter_by(email=email).first()
        new_car = Car(brand=brand, model=model, generation=generation,
                      series=series, modification=modification,
                      equipment=equipment, information=information,
                      price=price, user_id=user.id)
        db.session.add(new_car)
        db.session.commit()
        return redirect(url_for('main.my_cars'))

    return render_template(
        'add_car.html',
        title='my cars',
        menu=get_menu()
    )


@main.route("/profile/my_cars", methods=["POST", "GET"])
@login_required
def my_cars():
    user_id = current_user.id
    return render_template(
        'my_cars.html',
        title='my cars',
        menu=get_menu(),
        cars=get_user_cars(user_id)
    )


@main.route("/post/<id_post>",  methods=["POST", "GET"])
def post_id(id_post):
    post = Car.query.filter_by(id=id_post).first()
    return render_template(
        'car_post.html',
        title='post',
        menu=get_menu(),
        post=post
    )




