from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

FLASK_DEBUG = True
FLASK_APP = 'cars'

DB_USERNAME = 'postgres'
DB_PASSWORD = 'postgres'
DB_HOST = 'localhost'
DB_PORT = 5432
DB_NAME = 'postgres'
